package com.nespalier.controller;

import com.nespalier.commons.DonneesTech;
import com.nespalier.commons.controller.BaseController;
import com.nespalier.model.*;
import com.nespalier.services.ITransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/transaction")
public class TransactionController implements BaseController<Transaction> {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionController.class);

    @Autowired
    ITransactionService transactionService;

    @Override
    public ResponseEntity<Transaction> creerModifier(Transaction transaction) {
        return null;
    }
@CrossOrigin(origins = "*")
    @PostMapping(path = "/charge")
    public ResponseEntity<List<Transaction> > creerModifierCharge(@RequestBody Charge charge) {
        try {
            LOGGER.info("Create charge {}", charge.toString());
            List<Transaction>  response = transactionService.creerModifierCharge(charge);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
@CrossOrigin(origins = "*")
    @PostMapping(path = "/revenue")
    public ResponseEntity<List<Transaction>> creerModifierRevenue(@RequestBody Revenue revenue) {
        try {
            LOGGER.info("Create revenue {}", revenue.toString());
            List<Transaction>  response = transactionService.creerModifierRevenue(revenue);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
@CrossOrigin(origins = "*")
    @PostMapping(path = "/epargne")
    public ResponseEntity<List<Transaction>> creerModifierEpargne(@RequestBody Epargne epargne) {

            List<Transaction>  response = transactionService.creerModifierEpargne(epargne);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);

    }
@CrossOrigin(origins = "*")
    @Override
    public ResponseEntity<Transaction> trouveParClef(Long id) {
        try {
            Transaction response = transactionService.trouveParClef(id);
            LOGGER.info("transaction trouvé {}", response.toString());
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
@CrossOrigin(origins = "*")
    @Override
    public ResponseEntity<Void> supprimerParId(Long id) {
        try {
            Transaction transactionSupprime = transactionService.supprimerParId(id);
            LOGGER.info("transaction supprimé {}", transactionSupprime.toString());
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
@CrossOrigin(origins = "*")
    @Override
    public ResponseEntity<List<Transaction>> chercherTousLesElements() {
        List<Transaction> response = transactionService.chercherTousLesElements();
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
@CrossOrigin(origins = "*")
    @GetMapping(value = "/chargeFixe/{estAnnee}/{jour}/{mois}/{annee}")
    public ResponseEntity<Float> calculerChargeFixe(
            @PathVariable("estAnnee") boolean estAnnee,
            @PathVariable("jour") String jour,
            @PathVariable("mois") String mois,
            @PathVariable("annee") String annee) {
        Float response = transactionService.calculerChargesFixe(jour,mois,annee,estAnnee);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
@CrossOrigin(origins = "*")
    @GetMapping(value = "/chargeVar/{estAnnee}/{jour}/{mois}/{annee}")
    public ResponseEntity<Float> calculerChargeVariable(
            @PathVariable("estAnnee") boolean estAnnee,
            @PathVariable("jour") String jour,
            @PathVariable("mois") String mois,
            @PathVariable("annee") String annee) {
        Float response = transactionService.calculerChargesVariables(jour,mois,annee,estAnnee);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
@CrossOrigin(origins = "*")
    @GetMapping(value = "/chargeRelle/{estAnnee}/{jour}/{mois}/{annee}")
    public ResponseEntity<Float> calculerChargeVariableRelle(
            @PathVariable("estAnnee") boolean estAnnee,
            @PathVariable("jour") String jour,
            @PathVariable("mois") String mois,
            @PathVariable("annee") String annee) {
        Float response = transactionService.calculerChargesVariablesRelle(jour,mois,annee,estAnnee);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
@CrossOrigin(origins = "*")
    @GetMapping(value = "/revenue/{estAnnee}/{jour}/{mois}/{annee}")
    public ResponseEntity<Float> calculerRevenue(
            @PathVariable("estAnnee") boolean estAnnee,
            @PathVariable("jour") String jour,
            @PathVariable("mois") String mois,
            @PathVariable("annee") String annee) {
        Float response = transactionService.calculerRevenue(jour,mois,annee,estAnnee);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
@CrossOrigin(origins = "*")
    @GetMapping(value = "epargne/{estAnnee}/{jour}/{mois}/{annee}")
    public ResponseEntity<Float> calculerEpargne(
            @PathVariable("estAnnee") boolean estAnnee,
            @PathVariable("jour") String jour,
            @PathVariable("mois") String mois,
            @PathVariable("annee") String annee) {
        Float response = transactionService.calculerEpargne(jour,mois,annee,estAnnee);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
@CrossOrigin(origins = "*")
    @GetMapping(value = "charge/fixe/{annee}")
    public ResponseEntity<List<Float>> calculerChargesFixesAnnuelles(
            @PathVariable("annee") String annee) {
        List<Float> response = transactionService.recupererChargesFixeAnnee(annee);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
@CrossOrigin(origins = "*")
    @GetMapping(value = "charge/variables/{annee}")
    public ResponseEntity<List<Float>> calculerChargesVariablesAnnuelles(
            @PathVariable("annee") String annee) {
        List<Float> response = transactionService.recupererChargesVarAnnee(annee);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
@CrossOrigin(origins = "*")
    @GetMapping(value = "charge/reelles/{annee}")
    public ResponseEntity<List<Float>> calculerChargesReellesAnnuelles(
            @PathVariable("annee") String annee) {
        List<Float> response = transactionService.recupererChargesReelleAnnee(annee);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

@CrossOrigin(origins = "*")
    @GetMapping(value = "revenue/pardate/{estAnnee}/{jour}/{mois}/{annee}")
    public ResponseEntity<List<Transaction>> recupererRevenueParDate(
            @PathVariable String jour, @PathVariable String mois, @PathVariable String annee, @PathVariable boolean estAnnee) {
        List<Transaction> response = transactionService.recupererRevenueParDate(jour, mois, annee, estAnnee);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
@CrossOrigin(origins = "*")
    @GetMapping(value = "charge/fixe/pardate/{estAnnee}/{jour}/{mois}/{annee}")
    public ResponseEntity<List<Transaction>> recupererChargesFixeParDate(
            @PathVariable String jour, @PathVariable String mois, @PathVariable String annee, @PathVariable boolean estAnnee) {
        List<Transaction> response = transactionService.recupererChargesFixeParDate(jour, mois, annee, estAnnee);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
@CrossOrigin(origins = "*")
    @GetMapping(value = "charge/pardate/var/{estAnnee}/{jour}/{mois}/{annee}")
    public ResponseEntity<List<Transaction>> recupererChargesVariableParDate(
            @PathVariable String jour, @PathVariable String mois, @PathVariable String annee, @PathVariable boolean estAnnee) {
        List<Transaction> response = transactionService.recupererChargesVariableParDate(jour, mois, annee, estAnnee);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
@CrossOrigin(origins = "*")
    @GetMapping(value = "charge/pardate/reelle/{estAnnee}/{jour}/{mois}/{annee}")
    public ResponseEntity<List<Transaction>> recupererChargesVariableReellesParDate(
            @PathVariable String jour,
            @PathVariable String mois,
            @PathVariable String annee,
            @PathVariable boolean estAnnee) {
        List<Transaction> response = transactionService.recupererChargesVariablesRelleParDate(jour, mois, annee, estAnnee);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
@CrossOrigin(origins = "*")
    @GetMapping(value = "charge/pardate/epargne/{estAnnee}/{jour}/{mois}/{annee}")
    public ResponseEntity<List<Transaction>> recupererEpargneParDate(
            @PathVariable String jour,
            @PathVariable String mois,
            @PathVariable String annee,
            @PathVariable boolean estAnnee) {
        List<Transaction> response = transactionService.recupererEpargneParDate(jour, mois, annee, estAnnee);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

@CrossOrigin(origins = "*")
    @GetMapping(value = "charge/parposte/pardate/{estAnnee}/{jour}/{mois}/{annee}/{id}")
    public ResponseEntity<List<Charge>> recupererChargesVariableParDate(
            @PathVariable Long id,@PathVariable String jour, @PathVariable String mois, @PathVariable String annee, @PathVariable boolean estAnnee) {
        List<Charge> response = transactionService.recupererChargeParDateParPoste(id,jour, mois, annee, estAnnee);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
@CrossOrigin(origins = "*")
    @GetMapping(value = "charge/pardate/tot/{estAnnee}/{jour}/{mois}/{annee}")
    public ResponseEntity<List<Transaction>> recupererChargesParDate(
            @PathVariable String jour, @PathVariable String mois, @PathVariable String annee, @PathVariable boolean estAnnee) {
        List<Transaction> response = transactionService.recupererChargesParDate(jour, mois, annee, estAnnee);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }


    }
