package com.nespalier.controller;

import com.nespalier.commons.controller.BaseController;
import com.nespalier.model.Compte;
import com.nespalier.model.Livret;
import com.nespalier.model.Produit;
import com.nespalier.services.IProduitService;
import org.apache.tomcat.jni.Local;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/produit")
public class ProduitController implements BaseController<Produit> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProduitController.class);
    @Autowired
    IProduitService produitService;
@CrossOrigin(origins = "*") 
@Override
    public ResponseEntity<Produit> creerModifier(@RequestBody Produit produit) {
        try {
            LOGGER.info("Create produit {}", produit.toString());
            Produit response = produitService.creerModifier(produit);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
@CrossOrigin(origins = "*")
    @PostMapping(path = "/compte")
    public ResponseEntity<Produit> creerModifierCompte(@RequestBody Compte produit) {
        try {
            LOGGER.info("Create compte {}", produit.toString());
            Produit response = produitService.creerModifierCompte(produit);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
@CrossOrigin(origins = "*")
    @PostMapping(path = "/livret")
    public ResponseEntity<Produit> creerModifierLivret(@RequestBody Livret produit) {
        try {
            LOGGER.info("Create livret {}", produit.toString());
            Produit response = produitService.creerModifierLivret(produit);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
@CrossOrigin(origins = "*")
    @Override
    public ResponseEntity<Produit> trouveParClef(Long id) {
        try {
            Produit response = produitService.trouveParClef(id);
            LOGGER.info("produit trouvé {}", response.toString());
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
@CrossOrigin(origins = "*")
    @Override
    public ResponseEntity<Void> supprimerParId(Long id) {
        try {
            Produit produitSupprime = produitService.supprimerParId(id);
            LOGGER.info("produit supprimé {}", produitSupprime.toString());
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
@CrossOrigin(origins = "*")
    @Override
    public ResponseEntity<List<Produit>> chercherTousLesElements() {
        List<Produit> response = produitService.chercherTousLesElements();
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
@CrossOrigin(origins = "*")
    @GetMapping(value = "/{estMois}/{estAnnee}/{typeProduit}/{jour}/{mois}/{annee}")
    public ResponseEntity<Float> calculerSoldeParMoisReel(
                                                         @PathVariable("estMois") boolean estMois,
                                                         @PathVariable("estAnnee") boolean estAnnee,
                                                         @PathVariable("typeProduit") String typeProduit,
                                                         @PathVariable("jour") String jour,
                                                         @PathVariable("mois") String mois,
                                                         @PathVariable("annee") String annee) {
        Float response = produitService.calculSoldeParProduitReel(estMois,estAnnee,typeProduit,jour,mois,annee);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

}
