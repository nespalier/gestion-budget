package com.nespalier.controller;

import com.nespalier.commons.controller.BaseController;
import com.nespalier.model.Poste;
import com.nespalier.services.IPosteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/poste")
public class PosteController implements BaseController<Poste> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PosteController.class);
    @Autowired
    IPosteService posteService;
@CrossOrigin(origins = "*")
    @Override
    public ResponseEntity<Poste> creerModifier(@RequestBody Poste posteAcreer) {
        try {
            LOGGER.info("Create poste {}", posteAcreer.toString());
            Poste response = posteService.creerModifier(posteAcreer);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
@CrossOrigin(origins = "*")
    @Override
    public ResponseEntity<Poste> trouveParClef(Long id) {
        try {
            Poste response = posteService.trouveParClef(id);
            LOGGER.info("bien trouvé {}", response.toString());
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
@CrossOrigin(origins = "*")
    @Override
    public ResponseEntity<Void> supprimerParId(Long id) {
        try {
            Poste posteSupprime = posteService.supprimerParId(id);
            LOGGER.info("poste supprimé {}", posteSupprime.toString());
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
@CrossOrigin(origins = "*")
    @Override
    public ResponseEntity<List<Poste>> chercherTousLesElements() {
        List<Poste> response = posteService.chercherTousLesElements();
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
