package com.nespalier.services.impl;

import com.nespalier.commons.services.SharedServices;
import com.nespalier.model.*;
import com.nespalier.repo.IProduitRepo;
import com.nespalier.repo.ITransactionRepo;
import com.nespalier.services.ITransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionServiceImpl implements ITransactionService {

    @Autowired
    ITransactionRepo transactionRepo;
    @Autowired
    IProduitRepo produitRepo;

    @Override
    public Transaction creerModifier(Transaction objetAcreer) {
        objetAcreer.setComposteur(LocalDateTime.now());

        Transaction transaction = transactionRepo.save(objetAcreer);
        Produit produit = produitRepo.findById(objetAcreer.getProduit().getProduitTechnicalId()).get();
        if (CollectionUtils.isEmpty(produit.getListeTransactions())) {
            List<Transaction> transactions = new ArrayList<>();
            transactions.add(transaction);
            produit.setListeTransactions(transactions);
        } else {
            produit.getListeTransactions().add(transaction);
        }
        produitRepo.save(produit);
        return transaction;
    }

    @Override
    public Transaction trouveParClef(Long id) {
        return transactionRepo.findById(id).get();
    }

    @Override
    public List<Transaction> chercherTousLesElements() {
        return transactionRepo.findAll();
    }

    @Override
    public Transaction supprimerParId(Long id) {
        Transaction transaction = trouveParClef(id);
        transactionRepo.deleteById(id);
        return transaction;
    }

    @Override
    public List<Transaction> creerModifierCharge(Charge transaction) {
        Produit produit = produitRepo.findById(transaction.getProduit().getProduitTechnicalId()).get();
        if (Livret.class.equals(produit.getClass())) {
            produit.setSolde(produit.getSolde() - transaction.getMontant());
            produitRepo.save(produit);
        }

        transaction.setDate(SharedServices.convertToLocalDateTimeViaInstant(transaction.getDateTemp()));
        List<Transaction> transactions = new ArrayList<>();
        if (!transaction.isMensuel()) {
            transactions.add(creerModifier(transaction));
        } else {
            transactions = creerTransactionMensuel(transaction);
        }
        return transactions;
    }

    @Override
    public List<Transaction> creerModifierRevenue(Revenue transaction) {
        transaction.setDate(SharedServices.convertToLocalDateTimeViaInstant(transaction.getDateTemp()));
        List<Transaction> transactions = new ArrayList<>();
        if (!transaction.isMensuel()) {
            transactions.add(creerModifier(transaction));
        } else {
            transactions = creerTransactionMensuel(transaction);
        }
        return transactions;
    }

    @Override
    public List<Transaction> creerModifierEpargne(Epargne transaction) {
        transaction.setDate(SharedServices.convertToLocalDateTimeViaInstant(transaction.getDateTemp()));
        Produit produit = produitRepo.findById(transaction.getProduit().getProduitTechnicalId()).get();
        produit.setSolde(produit.getSolde() + transaction.getMontant());
        produitRepo.save(produit);

        List<Transaction> transactions = new ArrayList<>();
        if (!transaction.isMensuel()) {
            transactions.add(creerModifier(transaction));
        } else {
            transactions = creerTransactionMensuel(transaction);
        }

        return transactions;
    }

    @Override
    public List<Transaction> creerTransactionMensuel(Transaction transaction) {
        List<Transaction> result = new ArrayList<>();
        int mois = transaction.getDate().getMonthValue();
        int jour = transaction.getDate().getDayOfMonth();
        int annee = transaction.getDate().getYear();

        if (Charge.class.equals(transaction.getClass())) {

            while (mois <= 12) {
                Charge trans = Charge.getCLone(transaction);
                trans.setDate(LocalDateTime.of(annee, mois, jour, 0, 0));
                mois = mois + 1;
                creerModifier(trans);
                result.add(trans);
            }
        } else if (Revenue.class.equals(transaction.getClass())) {

            while (mois <= 12) {
                Revenue trans = Revenue.getCLone(transaction);
                trans.setDate(LocalDateTime.of(annee, mois, jour, 0, 0));
                mois = mois + 1;
                creerModifier(trans);
                result.add(trans);
            }
        } else if (Epargne.class.equals(transaction.getClass())) {
            while (mois <= 12) {
                Epargne trans = Epargne.getCLone(transaction);
                trans.setDate(LocalDateTime.of(annee, mois, jour, 0, 0));
                mois = mois + 1;
                creerModifier(trans);
                result.add(trans);
            }

        }
        return result;
    }

    public Float calculerChargesTotales(String jour, String mois, String annee, boolean estAnnee) {
        Float total = 0f;
        List<Transaction> t = transactionRepo.findAll().stream()
                .filter(trans -> Charge.class.equals(trans.getClass()) && !trans.isReelle() && SharedServices.comparerMoisAnnee(trans.getDate(), jour, mois, annee, estAnnee)).collect(Collectors.toList());
        for (Transaction trs : t) {
            total = total + trs.getMontant();
        }
        return total;
    }

    public List<Float> recupererChargesFixeAnnee(String annee) {
        List<Float> liste = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            Month mois = Month.of(i);
            Float result = this.calculerChargesFixe("31", mois.toString(), annee, false);
            liste.add(result);
        }
        return liste;
    }

    public List<Float> recupererChargesVarAnnee(String annee) {
        List<Float> liste = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            Month mois = Month.of(i);
            Float result = this.calculerChargesVariables("31", mois.toString(), annee, false);
            liste.add(result);
        }
        return liste;
    }

    public List<Float> recupererChargesReelleAnnee(String annee) {
        List<Float> liste = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            Month mois = Month.of(i);
            Float result = this.calculerChargesVariablesRelle("31", mois.toString(), annee, false);
            liste.add(result);
        }
        return liste;
    }

    public Float calculerChargesFixe(String jour, String mois, String annee, boolean estAnnee) {
        Float total = 0f;
        List<Transaction> t = transactionRepo.findAll().stream()
                .filter(trans -> Charge.class.equals(trans.getClass()) && trans.isFixe() && SharedServices.comparerMoisAnnee(trans.getDate(), jour, mois, annee, estAnnee)).collect(Collectors.toList());
        for (Transaction trs : t) {
            total = total + trs.getMontant();
        }
        return total;
    }

    public Float calculerChargesVariables(String jour, String mois, String annee, boolean estAnnee) {
        Float total = 0f;
        List<Transaction> t = transactionRepo.findAll().stream()
                .filter(trans -> Charge.class.equals(trans.getClass()) && !trans.isFixe() && !trans.isReelle() && SharedServices.comparerMoisAnnee(trans.getDate(), jour, mois, annee, estAnnee)).collect(Collectors.toList());
        for (Transaction trs : t) {
            total = total + trs.getMontant();
        }
        return total;
    }

    public Float calculerChargesVariablesRelle(String jour, String mois, String annee, boolean estAnnee) {
        Float total = 0f;
        List<Transaction> t = transactionRepo.findAll().stream()
                .filter(trans -> Charge.class.equals(trans.getClass()) && !trans.isFixe() && trans.isReelle() && SharedServices.comparerMoisAnnee(trans.getDate(), jour, mois, annee, estAnnee)).collect(Collectors.toList());
        for (Transaction trs : t) {
            total = total + trs.getMontant();
        }
        return total;
    }

    public Float calculerRevenue(String jour, String mois, String annee, boolean estAnnee) {
        Float total = 0f;
        List<Transaction> t = transactionRepo.findAll().stream()
                .filter(trans -> Revenue.class.equals(trans.getClass()) && SharedServices.comparerMoisAnnee(trans.getDate(), jour, mois, annee, estAnnee)).collect(Collectors.toList());
        for (Transaction trs : t) {
            total = total + trs.getMontant();
        }
        return total;
    }

    public Float calculerEpargne(String jour, String mois, String annee, boolean estAnnee) {
        Float total = 0f;
        List<Transaction> t = transactionRepo.findAll().stream()
                .filter(trans -> Epargne.class.equals(trans.getClass()) && SharedServices.comparerMoisAnnee(trans.getDate(), jour, mois, annee, estAnnee)).collect(Collectors.toList());
        for (Transaction trs : t) {
            total = total + trs.getMontant();
        }
        return total;
    }


    public List<Transaction> recupererChargesFixeParDate(String jour, String mois, String annee, boolean estAnnee) {
        List<Transaction> t = transactionRepo.findAll().stream()
                .filter(trans -> Charge.class.equals(trans.getClass()) && trans.isFixe() && SharedServices.comparerMoisAnnee(trans.getDate(), jour, mois, annee, estAnnee))
                .sorted(Comparator.comparing(Transaction::getDate))
                .collect(Collectors.toList());

        return t;
    }

    public List<Transaction> recupererChargesParDate(String jour, String mois, String annee, boolean estAnnee) {
        List<Transaction> t = transactionRepo.findAll().stream()
                .filter(trans -> Charge.class.equals(trans.getClass()) && SharedServices.comparerMoisAnnee(trans.getDate(), jour, mois, annee, estAnnee))
                .sorted(Comparator.comparing(Transaction::getDate))
                .collect(Collectors.toList());
        return t;
    }

    public List<Transaction> recupererChargesVariableParDate(String jour, String mois, String annee, boolean estAnnee) {
        List<Transaction> t = transactionRepo.findAll().stream()
                .filter(trans -> Charge.class.equals(trans.getClass()) && !trans.isFixe() && !trans.isReelle() && SharedServices.comparerMoisAnnee(trans.getDate(), jour, mois, annee, estAnnee))
                .sorted(Comparator.comparing(Transaction::getDate))
                .collect(Collectors.toList());
        return t;
    }

    public List<Transaction> recupererChargesVariablesRelleParDate(String jour, String mois, String annee, boolean estAnnee) {
        List<Transaction> t = transactionRepo.findAll().stream()
                .filter(trans -> Charge.class.equals(trans.getClass()) && !trans.isFixe() && trans.isReelle() && SharedServices.comparerMoisAnnee(trans.getDate(), jour, mois, annee, estAnnee))
                .sorted(Comparator.comparing(Transaction::getDate))
                .collect(Collectors.toList());
        return t;
    }

    public List<Transaction> recupererRevenueParDate(String jour, String mois, String annee, boolean estAnnee) {
        List<Transaction> t = transactionRepo.findAll().stream()
                .filter(trans -> Revenue.class.equals(trans.getClass()) && SharedServices.comparerMoisAnnee(trans.getDate(), jour, mois, annee, estAnnee))
                .sorted(Comparator.comparing(Transaction::getDate))
                .collect(Collectors.toList());
        return t;
    }

    public List<Transaction> recupererEpargneParDate(String jour, String mois, String annee, boolean estAnnee) {
        List<Transaction> t = transactionRepo.findAll().stream()
                .filter(trans -> Epargne.class.equals(trans.getClass()) && SharedServices.comparerMoisAnnee(trans.getDate(), jour, mois, annee, estAnnee))
                .sorted(Comparator.comparing(Transaction::getDate))
                .collect(Collectors.toList());
        return t;
    }

    public List<Charge> recupererChargeParDateParPoste(Long posteId, String jour, String mois, String annee, boolean estAnnee) {

        List<Charge> t = (List<Charge>) transactionRepo.findAll().stream();

        List<Charge> result = t.stream()
                .filter(c -> posteId == c.getPoste().getTechnicalId() && SharedServices.comparerMoisAnnee(c.getDate(), jour, mois, annee, estAnnee))
                .sorted(Comparator.comparing(Transaction::getDate))
                .collect(Collectors.toList());

        return result;
    }

}
