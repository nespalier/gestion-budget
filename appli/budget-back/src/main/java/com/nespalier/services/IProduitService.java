package com.nespalier.services;

import com.nespalier.commons.services.IBaseCrud;
import com.nespalier.model.Compte;
import com.nespalier.model.Livret;
import com.nespalier.model.Produit;

import java.time.Month;
import java.time.Year;

public interface IProduitService extends IBaseCrud<Produit> {

    Produit creerModifierLivret(Livret objetAcreer);

    Produit creerModifierCompte(Compte objetAcreer);

    Float calculSoldeParProduitReel(boolean estMois, boolean estAnnee, String typeProduit, String jour, String mois, String annee);

}
