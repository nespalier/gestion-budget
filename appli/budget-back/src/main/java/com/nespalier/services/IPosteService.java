package com.nespalier.services;

import com.nespalier.commons.services.IBaseCrud;
import com.nespalier.model.Poste;

public interface IPosteService extends IBaseCrud<Poste> {

}
