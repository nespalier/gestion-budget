package com.nespalier.services;

import com.nespalier.commons.DonneesTech;
import com.nespalier.commons.services.IBaseCrud;
import com.nespalier.model.Charge;
import com.nespalier.model.Epargne;
import com.nespalier.model.Revenue;
import com.nespalier.model.Transaction;

import java.util.List;

public interface ITransactionService extends IBaseCrud<Transaction> {

    List<Transaction>  creerModifierCharge(Charge transaction);

    List<Transaction>  creerModifierRevenue(Revenue transaction);

    List<Transaction> creerTransactionMensuel(Transaction transaction);

    List<Transaction>  creerModifierEpargne(Epargne transaction);

    Float calculerChargesFixe(String jour, String mois, String annee, boolean estAnnee);

    Float calculerChargesVariables(String jour, String mois, String annee, boolean estAnnee);

    Float calculerRevenue(String jour, String mois, String annee, boolean estAnnee);

    Float calculerEpargne(String jour, String mois, String annee, boolean estAnnee);

    Float calculerChargesVariablesRelle(String jour, String mois, String annee, boolean estAnnee);

    Float calculerChargesTotales(String jour, String mois, String annee, boolean estAnnee);

    List<Float> recupererChargesReelleAnnee(String annee);

    List<Float> recupererChargesVarAnnee(String annee);

    List<Float> recupererChargesFixeAnnee(String annee);

    List<Transaction> recupererChargesParDate(String jour, String mois, String annee, boolean estAnnee);

    List<Transaction> recupererEpargneParDate(String jour, String mois, String annee, boolean estAnnee);
    List<Transaction> recupererRevenueParDate(String jour, String mois, String annee, boolean estAnnee);
    List<Transaction> recupererChargesVariablesRelleParDate(String jour, String mois, String annee, boolean estAnnee);
    List<Transaction> recupererChargesVariableParDate(String jour, String mois, String annee, boolean estAnnee);
    List<Transaction> recupererChargesFixeParDate(String jour, String mois, String annee, boolean estAnnee);

    List<Charge> recupererChargeParDateParPoste(Long posteId, String jour, String mois, String annee, boolean estAnnee);


}
