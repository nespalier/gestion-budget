package com.nespalier.services.impl;

import antlr.StringUtils;
import com.nespalier.commons.Constantes;
import com.nespalier.commons.services.SharedServices;
import com.nespalier.model.*;
import com.nespalier.repo.IProduitRepo;
import com.nespalier.repo.ITransactionRepo;
import com.nespalier.services.IProduitService;
import com.nespalier.services.ITransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Year;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PorduitServiceImpl implements IProduitService {

    @Autowired
    IProduitRepo produitRepo;

    @Autowired
    ITransactionService transactionService;

    @Override
    public Produit creerModifier(Produit objetAcreer) {
        objetAcreer.setComposteur(LocalDateTime.now());
        return produitRepo.save(objetAcreer);
    }

    public Produit creerModifierLivret(Livret objetAcreer) {
        return creerModifier(objetAcreer);
    }

    public Produit creerModifierCompte(Compte objetAcreer) {
        return creerModifier(objetAcreer);
    }

    @Override
    public Float calculSoldeParProduitReel(boolean estMois, boolean estAnnee, String typeProduit, String jour, String mois, String annee) {
        Float total = 0f;

        if( Constantes.TYPE_PRODUIT.equals(typeProduit) && estMois) {
            List<Produit> produits = produitRepo.findAll();
            total = calculerSoldeReelParMois(produits, estAnnee, jour, mois, annee);
        } else if (Constantes.TYPE_COMPTE.equals(typeProduit) && estMois) {
            List<Produit> produits = produitRepo.findAll().stream().filter(
                    c -> Compte.class.equals(c.getClass())
            ).collect(Collectors.toList());
            total = calculerSoldeReelParMois(produits, estAnnee, jour, mois, annee);
        } else if (Constantes.TYPE_LIVRET.equals(typeProduit) && estMois){
            List<Produit> produits = produitRepo.findAll().stream().filter(
                    c -> Livret.class.equals(c.getClass())
            ).collect(Collectors.toList());
            total = calculerSoldeReelParMois(produits, estAnnee, jour, mois, annee);
        } else if( Constantes.TYPE_PRODUIT.equals(typeProduit) && estAnnee) {
            List<Produit> produits = produitRepo.findAll();
            total = calculerSoldeReelParAnnee(produits, estAnnee, jour, mois,annee);
        } else if (Constantes.TYPE_COMPTE.equals(typeProduit) && estAnnee) {
            List<Produit> produits = produitRepo.findAll().stream().filter(
                    c -> Compte.class.equals(c.getClass())
            ).collect(Collectors.toList());
            total = calculerSoldeReelParAnnee(produits, estAnnee, jour, mois,annee);
        } else if (Constantes.TYPE_LIVRET.equals(typeProduit) && estAnnee){
            List<Produit> produits = produitRepo.findAll().stream().filter(
                    c -> Livret.class.equals(c.getClass())
            ).collect(Collectors.toList());
            total = calculerSoldeReelParAnnee(produits, estAnnee, jour, mois, annee);
        }
        return total;
    }

    private Float calculerSoldeReelParMois(List<Produit> produits, boolean estAnnee, String jour, String mois, String annee) {
        Float total = 0f;
        Float totalCharge = 0f;
        Float totalRevenueReel = 0f;
        Float totalRevenuePrev = 0f;
        Float totalEpargne = 0f;
        List<Transaction> transactions = new ArrayList<>();

        for (Produit p: produits) {
            List<Transaction> trans = filtreParJourMoisEtAnnee(p.getListeTransactions(), jour, mois, annee, estAnnee);
            trans.forEach(
                    tr -> {

                            transactions.add(tr);

                    }
            );

        }

        Float chargeVar = transactionService.calculerChargesVariables(jour,mois,annee, estAnnee);
        Float chargeReelle = transactionService.calculerChargesVariablesRelle(jour,mois,annee, estAnnee);

        for (Transaction trs : transactions) {
            if (Charge.class.equals(trs.getClass())) {

                totalCharge = totalCharge - trs.getMontant();
            } else if (Revenue.class.equals(trs.getClass())) {
                if (trs.isReelle()) {
                    totalRevenueReel = totalRevenueReel + trs.getMontant();
                } else {
                    totalRevenuePrev = totalRevenuePrev + trs.getMontant();
                }
            }
        }

        total = totalRevenueReel > 0 ? total + totalRevenueReel : total + totalRevenuePrev;

        total = chargeReelle > chargeVar ? total + totalCharge + chargeVar : total + totalCharge + chargeVar;

        return total;
    }

    private Float calculerSoldeReelParAnnee(List<Produit> produits, boolean estAnnee, String jour, String mois, String annee) {
        Float total = 0f;
        List<Transaction> transactions = new ArrayList<>();

        for (Produit p: produits) {
            List<Transaction> trans = filtreParJourMoisEtAnnee(p.getListeTransactions(), jour, mois, annee, estAnnee).stream().filter(
                    t -> !t.isReelle()
            ).collect(Collectors.toList());
            trans.forEach(
                    tr -> transactions.add(tr)
            );

        }

        Float chargeVar = transactionService.calculerChargesVariables(jour,mois,annee, estAnnee);
        Float chargeReelle = transactionService.calculerChargesVariablesRelle(jour,mois,annee, estAnnee);

        for (Transaction trs : transactions) {
            if (Charge.class.equals(trs.getClass())) {
                total = total - trs.getMontant();
            } else {
                total = total + trs.getMontant();
            }
        }
        total = chargeReelle > chargeVar ? total + chargeVar : total;

        return total;
    }

    private List<Transaction> filtreParJourMoisEtAnnee(List<Transaction> transactions, String jour, String mois, String annee, boolean estAnnee) {
        return transactions.stream().filter(t -> SharedServices.comparerMoisAnnee(t.getDate(), jour, mois, annee, estAnnee)).collect(Collectors.toList());
    }

    @Override
    public Produit trouveParClef(Long id) {
        return produitRepo.findById(id).get();
    }

    @Override
    public List<Produit> chercherTousLesElements() {
        return produitRepo.findAll();
    }

    @Override
    public Produit supprimerParId(Long id) {
        Produit produit = this.trouveParClef(id);
        produitRepo.deleteById(id);
        return produit;
    }
}
