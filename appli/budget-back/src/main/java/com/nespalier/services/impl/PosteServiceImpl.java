package com.nespalier.services.impl;

import com.nespalier.model.Poste;
import com.nespalier.repo.IPosteRepo;
import com.nespalier.services.IPosteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class PosteServiceImpl implements IPosteService {
    @Autowired
    IPosteRepo posteRepo;

    @Override
    public Poste creerModifier(Poste objetAcreer) {
        objetAcreer.setDate(LocalDateTime.now());
        return posteRepo.save(objetAcreer);
    }

    @Override
    public Poste trouveParClef(Long id) {
        return posteRepo.findById(id).get();
    }

    @Override
    public List<Poste> chercherTousLesElements() {
        return posteRepo.findAll(Sort.by(Sort.Direction.ASC, "libelle"));
    }

    @Override
    public Poste supprimerParId(Long id) {
        Poste poste = this.trouveParClef(id);
        posteRepo.deleteById(id);
        return poste;
    }
}
