package com.nespalier.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Charge extends Transaction {

    @ManyToOne
    @JoinColumn(name = "technical_id")
    private Poste poste;

    public Poste getPoste() {
        return poste;
    }

    public void setPoste(Poste poste) {
        this.poste = poste;
    }

    public static Charge getCLone(Transaction t) {
        Charge transaction = new Charge();
        transaction.setProduit(t.getProduit());
        transaction.setDescription(t.getDescription());
        transaction.setFixe(t.isFixe());
        transaction.setMensuel(t.isMensuel());
        transaction.setMontant(t.getMontant());
        transaction.setPoste(((Charge) t).getPoste());
        return transaction;
    }

    @Override
    public String toString() {
        return "Charge{" +
                ", poste=" + poste +
                '}';
    }
}
