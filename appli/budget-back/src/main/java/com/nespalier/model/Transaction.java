package com.nespalier.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.nespalier.commons.BaseModel;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
// Cette annotation permet donc d'éviter les cycles
//@JsonIdentityInfo(generator= ObjectIdGenerators.IntSequenceGenerator.class)
public class Transaction extends BaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long transactionTechnicalId;

    private Float montant;

    private String description;

    private LocalDateTime date;

    private Date dateTemp;

    private LocalDateTime composteur;

    private boolean fixe;

    private boolean mensuel;

    private boolean reelle;

    @ManyToOne
    @JoinColumn(name = "produit_technical_id")
    private Produit produit;

    public Transaction() {
    }

    public boolean isReelle() {
        return reelle;
    }

    public void setReelle(boolean reelle) {
        this.reelle = reelle;
    }

    public boolean isFixe() {
        return fixe;
    }

    public void setFixe(boolean fixe) {
        this.fixe = fixe;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public Long getTransactionTechnicalId() {
        return transactionTechnicalId;
    }

    public void setTransactionTechnicalId(Long transactionTechnicalId) {
        this.transactionTechnicalId = transactionTechnicalId;
    }

    public Float getMontant() {
        return montant;
    }

    public void setMontant(Float montant) {
        this.montant = montant;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public LocalDateTime getComposteur() {
        return composteur;
    }

    public void setComposteur(LocalDateTime composteur) {
        this.composteur = composteur;
    }

    public boolean isMensuel() {
        return mensuel;
    }

    public void setMensuel(boolean mensuel) {
        this.mensuel = mensuel;
    }

    public Date getDateTemp() {
        return dateTemp;
    }

    public void setDateTemp(Date dateTemp) {
        this.dateTemp = dateTemp;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "technicalId=" + transactionTechnicalId +
                ", montant=" + montant +
                ", description='" + description + '\'' +
                ", date=" + date +
                ", produit=" + produit +
                '}';
    }
}
