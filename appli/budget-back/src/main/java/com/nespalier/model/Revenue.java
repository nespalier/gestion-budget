package com.nespalier.model;

import javax.persistence.Entity;

@Entity
public class Revenue extends  Transaction {
    public static Revenue getCLone(Transaction t) {
        Revenue transaction = new Revenue();
        transaction.setProduit(t.getProduit());
        transaction.setDescription(t.getDescription());
        transaction.setFixe(t.isFixe());
        transaction.setMensuel(t.isMensuel());
        transaction.setMontant(t.getMontant());
        return transaction;
    }
}
