package com.nespalier.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class Poste {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long technicalId;
    private String libelle;
    private LocalDateTime composteur;

    public Poste() {
    }

    public Poste(String libelle) {
        this.libelle = libelle;
        this.setTechnicalId(this.getTechnicalId());
    }

    public LocalDateTime getDate() {
        return composteur;
    }

    public void setDate(LocalDateTime date) {
        this.composteur = date;
    }

    public Long getTechnicalId() {
        return technicalId;
    }

    public void setTechnicalId(Long technicalId) {
        this.technicalId = technicalId;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public String toString() {
        return "Poste{" +
                "technicalId=" + technicalId +
                ", libelle='" + libelle + '\'' +
                '}';
    }
}
