package com.nespalier.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.nespalier.commons.BaseModel;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
// Cette annotation permet donc d'éviter les cycles
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class)
public class Produit extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long produitTechnicalId;

    private String numero;

    private String libelle;

    private Float solde = 0F;

    private Float soldePrev = 0F;

    private LocalDateTime composteur;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private List<Transaction> listeTransactions;

    public Produit() {
    }

    public Produit(String numero, String libelle, Float solde) {
        this.numero = numero;
        this.libelle = libelle;
        this.solde = solde;
    }

    public List<Transaction> getListeTransactions() {
        return listeTransactions;
    }

    public void setListeTransactions(List<Transaction> listeTransactions) {
        this.listeTransactions = listeTransactions;
    }

    public Long getProduitTechnicalId() {
        return produitTechnicalId;
    }

    public void setProduitTechnicalId(Long produitTechnicalId) {
        this.produitTechnicalId = produitTechnicalId;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Float getSolde() {
        return solde;
    }

    public void setSolde(Float solde) {
        this.solde = solde;
    }

    public Float getSoldePrev() {
        return soldePrev;
    }

    public void setSoldePrev(Float soldePrev) {
        this.soldePrev = soldePrev;
    }

    public LocalDateTime getComposteur() {
        return composteur;
    }

    public void setComposteur(LocalDateTime composteur) {
        this.composteur = composteur;
    }

    @Override
    public String toString() {
        return "Produit{" +
                "technicalId=" + produitTechnicalId +
                ", numero=" + numero +
                ", libelle='" + libelle + '\'' +
                ", solde=" + solde +
                '}';
    }
}
