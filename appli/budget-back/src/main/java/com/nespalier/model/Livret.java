package com.nespalier.model;

import javax.persistence.Entity;

@Entity
public class Livret extends Produit {

    private Float interet;

    public Float getInteret() {
        return interet;
    }

    public void setInteret(Float interet) {
        this.interet = interet;
    }
}
