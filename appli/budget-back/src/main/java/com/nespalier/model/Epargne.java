package com.nespalier.model;

import javax.persistence.Entity;

@Entity
public class Epargne extends Transaction {

    public static Epargne getCLone(Transaction t) {
        Epargne transaction = new Epargne();
        transaction.setProduit(t.getProduit());
        transaction.setDescription(t.getDescription());
        transaction.setFixe(t.isFixe());
        transaction.setMensuel(t.isMensuel());
        transaction.setMontant(t.getMontant());
        return transaction;
    }

}
