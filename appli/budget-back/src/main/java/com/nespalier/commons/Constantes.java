package com.nespalier.commons;

public class Constantes {

    public static final String TYPE_CHARGES = "CHARGE";
    public static final String TYPE_REVENUE = "REVENUE";
    public static final String TYPE_EPARGNES = "TYPE_EPARGNES";

    public static final String TYPE_COMPTE = "COMPTE";
    public static final String TYPE_LIVRET = "LIVRET";
    public static final String TYPE_PRODUIT = "PRODUIT";

}
