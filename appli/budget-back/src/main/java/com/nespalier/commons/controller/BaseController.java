package com.nespalier.commons.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface BaseController<T> {


    @PostMapping
    public ResponseEntity<T> creerModifier(@RequestBody T t);

    @GetMapping(value = {"/{id}"})
    public ResponseEntity<T> trouveParClef(@PathVariable("id") Long id);

    @DeleteMapping(value = {"/{id}"})
    public ResponseEntity<Void> supprimerParId(@PathVariable("id") Long id);

    @GetMapping
    public ResponseEntity<List<T>> chercherTousLesElements();

}
