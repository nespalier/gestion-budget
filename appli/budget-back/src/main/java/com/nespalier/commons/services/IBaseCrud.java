package com.nespalier.commons.services;

import java.util.List;

public interface IBaseCrud<T> {

    T creerModifier(T objetAcreer);

    T trouveParClef(Long id);

    List<T> chercherTousLesElements();

    T supprimerParId(Long id);

}
