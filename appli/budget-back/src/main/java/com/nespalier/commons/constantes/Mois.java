package com.nespalier.commons.constantes;

public final class Mois {
    public static final int JANVIER = 1;
    public static final int FEVRIER = 2;
    public static final int MARS = 3;
    public static final int AVRIL = 4;
    public static final int MAI = 5;
    public static final int JUIN = 6;
    public static final int JUILLET = 7;
    public static final int AOÛT = 8;
    public static final int SEPTEMBRE = 9;
    public static final int OCTOBRE = 10;
    public static final int NOVEMBRE = 11;
    public static final int DECEMBRE = 12;
}
