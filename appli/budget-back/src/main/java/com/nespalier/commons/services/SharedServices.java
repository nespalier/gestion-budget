package com.nespalier.commons.services;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class SharedServices {

    public static boolean comparerMoisAnnee(LocalDateTime date, String jour, String mois, String year, boolean estAnnee) {
        String moisAcomparer = date.getMonth().toString();
        Integer anneeAcomparer = date.getYear();
        Integer jourAcomparer = date.getDayOfMonth();

        return !estAnnee ? anneeAcomparer.toString().equals(year) && moisAcomparer.equals(mois) && jourAcomparer <= Integer.parseInt(jour)
                : anneeAcomparer.toString().equals(year);
    }

    public static LocalDateTime convertToLocalDateTimeViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

}
