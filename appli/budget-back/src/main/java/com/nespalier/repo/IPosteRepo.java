package com.nespalier.repo;

import com.nespalier.model.Poste;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPosteRepo extends JpaRepository<Poste, Long> {
}
