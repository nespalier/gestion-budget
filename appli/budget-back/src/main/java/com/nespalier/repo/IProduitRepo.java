package com.nespalier.repo;

import com.nespalier.model.Produit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IProduitRepo extends JpaRepository<Produit,Long> {
}
