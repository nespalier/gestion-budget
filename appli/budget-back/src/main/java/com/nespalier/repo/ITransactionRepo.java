package com.nespalier.repo;

import com.nespalier.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITransactionRepo extends JpaRepository<Transaction,Long> {
}
