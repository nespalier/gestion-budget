import { Identifiers } from '@angular/compiler';
import { stringify } from '@angular/compiler/src/util';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Annee } from 'app/shared/annee';
import { Charge } from 'app/shared/classes-vo/charge';
import { Transaction } from 'app/shared/classes-vo/transaction';
import { Constantes } from 'app/shared/constantes';
import { Mois } from 'app/shared/mois';
import { CommunService } from 'app/shared/services/commun.service';
import { ProduitService } from 'app/shared/services/produit.service';
import { TransactionService } from 'app/shared/services/transaction.service';
import Chart from 'chart.js';
import { PosteVo } from '../admin/classes-vo/poste-vo';
import { PosteService } from '../admin/services/poste.service';


@Component({
  selector: 'dashboard-cmp',
  moduleId: module.id,
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.css']
})

export class DashboardComponent implements OnInit {
  @ViewChild('pieChart') public canvasPie: ElementRef;
  public canvas: any;
  public ctx;
  public chartColor;
  public chartEmail;
  public chartHours;
  gauge: Chart;
  soldeReel: number;
  soldePrev: number;
  soldeLivret: number;
  chargeReelle: number;
  chargeVar: number;
  chargeFixe: number;
  revenue: number;
  epargne: number;
  date: Date = new Date();
  estMois: boolean = true;
  estAnnee: boolean;
  formPeriode: FormGroup;
  listeMois: any;
  listeAnnee: any;
  listeJour: number[];
  miseAjour: string;
  texteVar: string;
  signe: string;
  signePrev: string;
  signeEcart: string;
  listeChargesRellesParMois: number[] = [];
  listeChargesFixeParMois: number[] = [];
  listeChargesVarParMois: number[] = [];
  listeChargesEpargnesParMois: number[] = [];
  pieOptions: any;
  pieData: any;
  tauxEndettement: number;
  listeCharges: Charge[];
  listePoste: PosteVo[];

  dataFirst = {
    data: [],
    fill: false,
    borderColor: '#fbc658',
    backgroundColor: 'transparent',
    pointBorderColor: '#fbc658',
    pointRadius: 4,
    pointHoverRadius: 4,
    pointBorderWidth: 8,
  };

  dataSecond = {
    data: [],
    fill: false,
    borderColor: '#51CACF',
    backgroundColor: 'transparent',
    pointBorderColor: '#51CACF',
    pointRadius: 4,
    pointHoverRadius: 4,
    pointBorderWidth: 8
  };

  speedData = {
    labels: ["Jan", "Fev", "Mar", "Avr", "Mai", "Juin", "Juil", "Août", "Septembre", "Oct", "Nov", "Dec"],
    datasets: []
  };
  chargesTotales: number;
  chargeReelleMois: number;
  afficherDetail: boolean = false;
  texteBtnAfficher: string = 'Afficher';
  listeRevenue: Transaction[];
  revenueReel: number;
  verifOk: boolean = true;
  revenuePrev: number;

  constructor(
    private readonly produitService: ProduitService,
    private readonly transactionService: TransactionService,
    private readonly communService: CommunService,
    private readonly posteService: PosteService,
    private fc: FormBuilder
  ) {

    this.formPeriode = this.fc.group({
      libelle: [this.communService.getMois(this.date.getMonth())],
      annee: [this.date.getFullYear().toString()],
      jour: [this.date.getDate()],
      compte: [Constantes.TYPE_COMPTE],
      radio: ["mois"]
    });
  }

  ngOnInit() {
    this.texteVar = "de mois";
    this.calculerSoldeCustom();
    this.calculerChargesVariables();
    this.calculerChargesFixe();
    this.calculerChargesReelle();
    this.calculerEpargne();
    this.calculerRevenue();
    this.calculerSoldePrev();
    this.initMois();
    this.initAnnee();
    this.initListeJour();
    this.calculerChargesReelleParMois();
    this.calculerChargesReelleMois();
    this.recupererListeCharge();
    this.chargerListePoste();
    this.recupererListeRevenue();
    this.recupererSoldeLivret();
    this.calculRevenuePrev();

    this.miseAjour = this.date.toLocaleString();

    this.chartColor = "#FFFFFF";

    this.canvas = document.getElementById("chartHours");
    this.ctx = this.canvas.getContext("2d");

    this.chartHours = new Chart(this.ctx, {
      type: 'line',

      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct"],
        datasets: [{
          borderColor: "#6bd098",
          backgroundColor: "#6bd098",
          pointRadius: 0,
          pointHoverRadius: 0,
          borderWidth: 3,
          data: [300, 310, 316, 322, 330, 326, 333, 345, 338, 354]
        },
        {
          borderColor: "#f17e5d",
          backgroundColor: "#f17e5d",
          pointRadius: 0,
          pointHoverRadius: 0,
          borderWidth: 3,
          data: [320, 340, 365, 360, 370, 385, 390, 384, 408, 420]
        },
        {
          borderColor: "#fcc468",
          backgroundColor: "#fcc468",
          pointRadius: 0,
          pointHoverRadius: 0,
          borderWidth: 3,
          data: [370, 394, 415, 409, 425, 445, 460, 450, 478, 484]
        }
        ]
      },
      options: {
        legend: {
          display: false
        },

        tooltips: {
          enabled: false
        },

        scales: {
          yAxes: [{

            ticks: {
              fontColor: "#9f9f9f",
              beginAtZero: false,
              maxTicksLimit: 5,
              //padding: 20
            },
            gridLines: {
              drawBorder: false,
              zeroLineColor: "#ccc",
              color: 'rgba(255,255,255,0.05)'
            }

          }],

          xAxes: [{
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: 'rgba(255,255,255,0.1)',
              zeroLineColor: "transparent",
              display: false,
            },
            ticks: {
              padding: 20,
              fontColor: "#9f9f9f"
            }
          }]
        },
      }
    });




  }
  chargerListePoste() {
    this.posteService.listePostes().subscribe(
      res => {
        this.listePoste = res;
      }
    );
  }

  pieChart(charge: number, revenue: number) {
    this.canvas = document.getElementById("chartEmail");
    this.ctx = this.canvas.getContext("2d");
    this.pieData = {
      labels: [1, 2, 3],
      datasets: [{
        label: "Emails",
        pointRadius: 0,
        pointHoverRadius: 0,
        backgroundColor: [
          '#6bd098 ',
          '#ef8157',
        ],
        borderWidth: 0,
        data: [charge, revenue]
      }],
    };

    this.pieOptions = {

      legend: {
        display: false
      },

      pieceLabel: {
        render: 'percentage',
        fontColor: ['white'],
        precision: 2
      },

      tooltips: {
        enabled: false
      },

      plugins: {
        datalabels: {
          formatter: (value, ctx) => {

            let sum = 0;
            let dataArr = ctx.chart.data.datasets[0].data;
            dataArr.map(data => {
              sum += data;
            });
            let percentage = (value * 100 / sum).toFixed(2) + "%";
            return percentage;


          },
          color: '#fff',
        }
      },


      scales: {
        yAxes: [{

          ticks: {
            display: false
          },
          gridLines: {
            drawBorder: false,
            zeroLineColor: "transparent",
            color: 'rgba(255,255,255,0.05)'
          }

        }],

        xAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(255,255,255,0.1)',
            zeroLineColor: "transparent"
          },
          ticks: {
            display: false,
          }
        }]
      },
    }

    this.chartEmail = new Chart(this.ctx, {
      type: 'pie',
      data: this.pieData,

      options: this.pieOptions
    });
  }

  chargerGraphique() {
    this.speedData = {
      labels: ["Jan", "Fev", "Mar", "Avr", "Mai", "Juin", "Juil", "Août", "Septembre", "Oct", "Nov", "Dec"],
      datasets: [this.dataSecond, this.dataFirst]
    };
    var speedCanvas = document.getElementById("speedChart");

    var chartOptions = {
      legend: {
        display: false,
        position: 'top'
      }
    };

    var lineChart = new Chart(speedCanvas, {
      type: 'line',
      hover: false,
      data: this.speedData,
      options: chartOptions
    });
  }

  recupererListeCharge() {
    const estmois = !(this.formPeriode.value.radio === 'annee' ? true : false);
    const estAnnee = this.formPeriode.value.radio === 'annee' ? true : false;
    const jour = "31";
    const mois = this.formPeriode.value.libelle;
    const annee = this.formPeriode.value.annee;
    this.transactionService.recupererListeChargesParDate(estAnnee, jour, mois, annee).subscribe(
      res => {
        this.listeCharges = res;
      }
    )
  }

  calculerSoldePrev(): number {
    return this.soldePrev = this.revenueReel > this.chargeVar && this.formPeriode.value.radio !== 'annee' ? this.revenueReel - this.chargesTotales - this.epargne : this.revenuePrev - this.chargesTotales - this.epargne;
  }

  calculerSolde(estMois: boolean, estAnnee: boolean, typeProduit: string, jour: number, mois: string, annee: string) {
    const jourString = jour.toString();
    this.produitService.calculerSolde(estMois, estAnnee, typeProduit, jourString, mois, annee).subscribe(
      res => {
        this.soldeReel = res;
      }
    )
  }

  calculerSoldeReel(): number {
    return this.soldeReel - this.epargne;
  }

  calculerChargesTotales(): number {
    return this.chargeFixe + this.chargeReelle;
  }

  calculerChargesTotalesPrev(): number {
    return this.chargesTotales = this.chargeReelle < this.chargeVar ? this.chargeFixe + this.chargeVar : this.chargeFixe + this.chargeReelle;
  }

  calculerChargesVariables() {
    const estAnnee = this.formPeriode.value.radio === 'annee' ? true : false;
    const jour = "31";
    const mois = this.formPeriode.value.libelle;
    const annee = this.formPeriode.value.annee;
    this.transactionService.calculerChargeVar(estAnnee, jour, mois, annee).subscribe(
      res => {
        this.chargeVar = res;
      }
    )
  }

  calculerChargesFixe() {
    const estAnnee = this.formPeriode.value.radio === 'annee' ? true : false;
    const jour = "31";
    const mois = this.formPeriode.value.libelle;
    const annee = this.formPeriode.value.annee;
    this.transactionService.calculerChargeFixe(estAnnee, jour, mois, annee).subscribe(
      res => {
        this.chargeFixe = res;
      }
    )
  }

  calculerChargesReelle() {
    const estAnnee = this.formPeriode.value.radio === 'annee' ? true : false;
    const jour = this.formPeriode.value.jour;
    const mois = this.formPeriode.value.libelle;
    const annee = this.formPeriode.value.annee;
    this.transactionService.calculerChargeVarReelle(estAnnee, jour, mois, annee).subscribe(
      res => {
        this.chargeReelle = res;
      }
    )

  }

  calculerChargesReelleMois() {
    const estAnnee = this.formPeriode.value.radio === 'annee' ? true : false;
    const jour = "31";
    const mois = this.formPeriode.value.libelle;
    const annee = this.formPeriode.value.annee;
    this.transactionService.calculerChargeVarReelle(estAnnee, jour, mois, annee).subscribe(
      res => {
        this.chargeReelleMois = res;
      }
    )

  }

  calculTauxEndettement(): number {
    return this.tauxEndettement = Math.round(this.chargeFixe && this.revenuePrev ? (this.chargeFixe / this.revenuePrev) * 100 : 0);
  }

  calculerChargesReelleParMois() {
    const annee = this.formPeriode.value.annee;
    for (const mois of Mois) {
      this.transactionService.recupererListeChargesReelles(annee).subscribe(
        res => {
          this.dataSecond.data = res;
          if (this.dataSecond.data.length === 12) {
            this.transactionService.recupererListeChargesVar(annee).subscribe(
              r => {
                this.dataFirst.data = r;
                if (this.dataFirst.data.length === 12) {
                  this.chargerGraphique();
                }
              }
            )
          }
        }
      )
    }

  }


  calculerEpargne() {
    const estAnnee = this.formPeriode.value.radio === 'annee' ? true : false;
    const jour = this.formPeriode.value.jour;
    const mois = this.formPeriode.value.libelle;
    const annee = this.formPeriode.value.annee;
    this.transactionService.calculerEpargne(estAnnee, jour, mois, annee).subscribe(
      res => {
        this.epargne = res;
      }
    )
  }

  calculerRevenue() {
    const estAnnee = this.formPeriode.value.radio === 'annee' ? true : false;
    const jour = "31";
    const mois = this.formPeriode.value.libelle;
    const annee = this.formPeriode.value.annee;
    this.transactionService.calculerRevenue(estAnnee, jour, mois, annee).subscribe(
      res => {
        this.revenue = res;
        if (res) {
          const charge = this.calculerChargesTotales();
          const totales = res + charge;
          const percRevenue = res / totales;
          const percCharge = charge / totales;
          charge > 0 ? this.pieChart(percCharge, percRevenue) : this.pieChart(percCharge, percRevenue);
        } else {
          this.revenue = 0;
          this.pieChart(0, 0);
        }
      }
    )
  }

  calculerRevenuePrev(): number {
    return this.revenue;
  }

  calculerSoldeCustom() {
    const estMois = !(this.formPeriode.value.radio === 'annee' ? true : false);
    const estAnnee = this.formPeriode.value.radio === 'annee' ? true : false;
    const typeProduit = this.formPeriode.value.compte;
    const jour = this.formPeriode.value.jour;
    const mois = this.formPeriode.value.libelle;
    const annee = this.formPeriode.value.annee;
    if (typeProduit != "LIVRET") {
      this.produitService.calculerSolde(estMois, estAnnee, typeProduit, jour, mois, annee).subscribe(
        res => {
          this.soldeReel = res;
          this.initMiseAjour();
        }
      )
    } else {
      this.soldeReel = this.soldeLivret;
      this.initMiseAjour();
    }

  }

  calculerEcart(): number {
    return this.chargeReelle && this.chargeVar ? (this.chargeReelle - this.chargeVar)
      : (this.chargeReelle - this.chargeVar);
  }

  colorerSolde(): string {
    this.signe = this.soldeReel <= 0 ? '' : '+';
    return this.soldeReel < 0 ? 'text-danger' : 'text-success';
  }

  colorerSoldePrev(): string {
    this.signePrev = this.soldePrev <= 0 ? '' : '+';
    return this.soldePrev < 0 ? 'text-danger' : 'text-success';
  }

  colorerEcart(): string {
    const num = this.calculerEcart();
    this.signeEcart = num > 0 ? '+' : '';
    return num < 0 ? 'text-success' : 'text-danger';
  }

  colorerTauxEndettement(): string {
    let style: string;
    let number = this.calculTauxEndettement();
    if (number < 20 && number >= 0) {
      style = 'text-success';
    } else if (number >= 20 && number <= 33) {
      style = 'text-warning';
    } else if (number > 33) {
      style = 'text-danger';
    }
    return style;
  }

  initMois() {
    this.listeMois = Mois;
  }

  initAnnee() {
    this.listeAnnee = Annee;
  }

  initListeJour() {
    this.listeJour = this.communService.getListeJour();
  }

  cacherJourMois(): boolean {
    return this.formPeriode.value.radio === 'annee' ? false : true;
  }

  initMiseAjour() {
    const majDate = new Date();
    this.miseAjour = majDate.toLocaleString();
  }

  valider() {
    this.texteVar = this.formPeriode.value.radio === 'annee' ? "d'année" : 'de mois';
    this.calculerSoldeCustom();
    this.calculerChargesVariables();
    this.calculerChargesFixe();
    this.calculerChargesReelle();
    this.calculerEpargne();
    this.calculerRevenue();
    this.calculerSoldePrev();
    this.calculerChargesReelleParMois();
    this.calculerChargesTotales();
    this.calculerChargesReelleMois();
    this.recupererListeRevenue();
    this.recupererListeCharge();
    this.calculRevenuePrev();
    this.verifOk = this.formPeriode.value.radio !== 'annee';
  }

  calculerChargeParDateParPoste(id: number, estRelle: boolean): number {
    return this.listeCharges?.filter(
      charge => charge.poste.technicalId === id && (charge.reelle === estRelle || charge.fixe)
    ).reduce((sum, current) => sum + current.montant, 0);
  }

  triChargeParDateParPoste(id: number, estRelle: boolean): Charge[] {
    return this.listeCharges?.filter(
      charge => charge.poste.technicalId === id && (charge.reelle === estRelle || charge.fixe)
    );
  }

  afficher(): boolean {
    this.texteBtnAfficher = this.afficherDetail ? 'Afficher' : 'Masquer';
    return this.afficherDetail = !this.afficherDetail;
  }


  recupererListeRevenue() {
    const estAnnee = this.formPeriode.value.radio === 'annee' ? true : false;
    const jour = "31";
    const mois = this.formPeriode.value.libelle;
    const annee = this.formPeriode.value.annee;
    this.transactionService.recupererListeRevenueParDate(estAnnee, jour, mois, annee).subscribe(
      res => {
        this.listeRevenue = res.filter(revenue => revenue.reelle);
        this.revenueReel = res.filter(revenue => revenue.reelle)
          .reduce((sum, current) => sum + current.montant, 0);
      }
    )
  }

  calculRevenueReel(): number {
    return this.revenueReel;
  }

  calculRevenuePrev() {
    const estAnnee = this.formPeriode.value.radio === 'annee' ? true : false;
    const jour = "31";
    const mois = this.formPeriode.value.libelle;
    const annee = this.formPeriode.value.annee;
    this.transactionService.recupererListeRevenueParDate(estAnnee, jour, mois, annee).subscribe(
      res => {
        this.revenuePrev = res.filter(revenue => !revenue.reelle)
          .reduce((sum, current) => sum + current.montant, 0);
      }
    )
  }

  recupererSoldeLivret() {
    this.produitService.listeProduits().subscribe(
      res => {
        this.soldeLivret = res.filter(p => p.solde)
          .reduce((sum, current) => sum + current.solde, 0);
      }
    )
  }


  depenseReellesVsPrev(): boolean {
    return this.chargeReelle > this.chargeVar;
  }

  suiviDepensesReelles(id: number): number {
    const reelles = this.listeCharges?.filter(
      charge => charge.poste.technicalId === id && (charge.reelle || charge.fixe)
    ).reduce((sum, current) => sum + current.montant, 0);
    const prev = this.listeCharges?.filter(
      charge => charge.poste.technicalId === id && (!charge.reelle || charge.fixe)
    ).reduce((sum, current) => sum + current.montant, 0);
    return prev > 0 ? Math.round((reelles / prev) * 100) : 0;
  }

  partDepensesReelles(id: number): number {
    const reelles = this.listeCharges?.filter(
      charge => charge.poste.technicalId === id && (charge.reelle || charge.fixe)
    ).reduce((sum, current) => sum + current.montant, 0);
    return this.chargesTotales > 0 ? Math.round((reelles / this.chargesTotales) * 100) : 0;
  }

  colorerProgressBar(id: number): string {
    let couleur = '';
    if (this.suiviDepensesReelles(id) < 50) {
      couleur = ' progress-bar-success';
    } else if (this.suiviDepensesReelles(id) > 100) {
      couleur = ' progress-bar-danger'
    } else if (this.suiviDepensesReelles(id) > 70) {
      couleur = ' progress-bar-warning';
    }
    return couleur;
  }

  colorerPartDepenses(id: number): string {
    let couleur = '';
    const part = this.partDepensesReelles(id);
    if (part < 50) {
      couleur = ' text-success';
    } else if (part >= 70) {
      couleur = ' text-danger'
    } else if (part >= 50 || part < 70) {
      couleur = ' text-warning';
    }
    return couleur;
  }

  convertNumberToFormatString(number: number): string {
    return this.communService.numberToFormatlocale(number);
  }

}
