import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { PosteVo } from '../classes-vo/poste-vo';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PosteService {

  path = environment.path + 'poste';

  constructor(private http: HttpClient, private route: Router) { }

  ajouterModifierPoste(posteVo: PosteVo) {
    return this.http.post<PosteVo>(this.path, posteVo);
  }

  listePostes() {
    return this.http.get<PosteVo[]>(this.path);
  }

  supprimerPosteParId(id: number) {
    return this.http.delete(this.path + "/" + id);
  }

  recuperePosteParId(id: number) {
    return this.http.get<PosteVo>(this.path + "/" + id);
  }

}
