import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Compte } from 'app/shared/classes-vo/compte';
import { Livret } from 'app/shared/classes-vo/Livret';
import { Produit } from 'app/shared/classes-vo/produit';
import { Constantes } from 'app/shared/constantes';
import { CommunService } from 'app/shared/services/commun.service';
import { ProduitService } from 'app/shared/services/produit.service';
import { PosteVo } from './classes-vo/poste-vo';
import { PosteService } from './services/poste.service';

@Component({
    selector: 'admin-cmp',
    moduleId: module.id,
    templateUrl: 'admin.component.html',
    styleUrls: ['admin.css']
})

export class AdminComponent implements OnInit {
    formAjoutModifPoste: FormGroup;
    formAjoutModifProduit: FormGroup;
    poste: PosteVo = new PosteVo();
    compte: Compte = new Compte();
    livret: Livret = new Livret();
    produits: Produit[] = [];
    listePostes: PosteVo[] = [];
    nombrePoste: number;
    nombreObsolete: number;
    postesObsolete: PosteVo[] = [];
    nombreProduit: number;

    constructor(
        private readonly posteService: PosteService,
        private readonly produitService: ProduitService,
        private fc: FormBuilder,
        private readonly communService: CommunService
    ) {
        this.formAjoutModifPoste = this.fc.group({
            id: [null],
            libelle: ["", [Validators.required]],
        });

        this.formAjoutModifProduit = this.fc.group({
            id: [null],
            type: ["", [Validators.required]],
            numero: [null, [Validators.required]],
            interet: [null],
            libelle: ["", [Validators.required]],
        });
    }
    ngOnInit() {
        this.initDonnees();
    }

    initDonnees() {
        this.initDonneesPoste();
        this.initDonneesProduit()
    }

    initDonneesPoste() {
        this.posteService.listePostes().subscribe(
            res => {
                this.listePostes = res;
                this.nombrePoste = res.length;
                this.postesObsolete = res.filter(r => r.libelle.trim().length === 0);
                this.nombreObsolete = this.postesObsolete.length > 0 ? this.postesObsolete.length : 0;
            }
        )
    }

    initDonneesProduit() {
        this.produitService.listeProduits().subscribe(
            res => {
                this.produits = res;
                this.nombreProduit = res.length;
            }
        )
    }

    valider() {
        this.poste.libelle = this.formAjoutModifPoste.value.libelle;
        this.poste.libelle = this.communService.titleCaseWord(this.poste.libelle);
        this.posteService.ajouterModifierPoste(this.poste).subscribe(
            r => {
                if (r) {
                    const message = 'Le poste: ' + r.libelle + ' a bien été créé.'
                    this.communService.genererNotification(message, Constantes.SUCCESS, Constantes.TOP, Constantes.RIGHT);
                    this.initDonnees();
                    this.formAjoutModifPoste.reset();
                    this.formAjoutModifPoste.value.id = null;
                }
            }
        )
    }

    validerProduit() {
        this.compte.numero = this.formAjoutModifProduit.value.type === Constantes.TYPE_COMPTE_TXT ? this.formAjoutModifProduit.value.numero : '';
        this.compte.libelle = this.formAjoutModifProduit.value.type === Constantes.TYPE_COMPTE_TXT ? this.communService.titleCaseWord(this.formAjoutModifProduit.value.libelle) : '';
        this.livret.numero = this.formAjoutModifProduit.value.type === Constantes.TYPE_LIVRET_TXT ? this.formAjoutModifProduit.value.numero : '';
        this.livret.libelle = this.formAjoutModifProduit.value.type === Constantes.TYPE_LIVRET_TXT ? this.communService.titleCaseWord(this.formAjoutModifProduit.value.libelle) : '';
        this.livret.interet = this.formAjoutModifProduit.value.type === Constantes.TYPE_LIVRET_TXT ? this.formAjoutModifProduit.value.interet : '';


        if (this.formAjoutModifProduit.value.type === Constantes.TYPE_COMPTE_TXT) {
            this.enregistrerCompte(this.compte);
        } else {
            this.enregistrerLivret(this.livret);
        }


    }

    enregistrerCompte(compte: Compte) {
        this.produitService.ajouterModifierCompte(compte).subscribe(
            r => {
                if (r) {
                    const message = 'Le comtpe: ' + r.libelle + ' a bien été créé.'
                    this.communService.genererNotification(message, Constantes.SUCCESS, Constantes.TOP, Constantes.RIGHT);
                    this.initDonnees();
                    this.formAjoutModifProduit.reset();
                    this.formAjoutModifProduit.value.id = null;
                }
            }
        )
    }

    enregistrerLivret(livret: Livret) {
        this.produitService.ajouterModifierLivret(livret).subscribe(
            r => {
                if (r) {
                    const message = 'Le livret: ' + r.libelle + ' a bien été créé.'
                    this.communService.genererNotification(message, Constantes.SUCCESS, Constantes.TOP, Constantes.RIGHT);
                    this.initDonnees();
                    this.formAjoutModifProduit.reset();
                    this.formAjoutModifProduit.value.id = null;
                }
            }
        )
    }

    modifier(poste: PosteVo) {
        this.formAjoutModifPoste.value.id = poste.technicalId;
        this.formAjoutModifPoste.value.libelle = poste.libelle;
    }

    supprimer(id: number) {
        this.posteService.supprimerPosteParId(id).subscribe(
            r => {
                const message = 'Le poste: a bien été supprimé.'
                this.communService.genererNotification(message, Constantes.SUCCESS, Constantes.TOP, Constantes.RIGHT);
                this.initDonnees();
            }
        )
    }

    modifierProduit(produit: any) {
        this.formAjoutModifProduit.value.id = produit.produitTechnicalId;
        this.formAjoutModifProduit.value.numero = produit.numero;
        this.formAjoutModifProduit.value.libelle = produit.libelle;
        this.formAjoutModifProduit.value.interet = produit.interet;
    }

    supprimerProduit(id: number) {
        this.produitService.supprimerproduitParId(id).subscribe(
            r => {
                const message = 'Le produit: a bien été supprimé.'
                this.communService.genererNotification(message, Constantes.SUCCESS, Constantes.TOP, Constantes.RIGHT);
                this.initDonnees();
            }
        )
    }

    activerBoutonValider(): boolean {
        return !this.communService.formulaireValide(this.formAjoutModifPoste);
    }

    activerBoutonValiderProduit(): boolean {
        return !this.communService.formulaireValide(this.formAjoutModifProduit);
    }

    afficherInteret(): boolean {
        return this.formAjoutModifProduit.value.type === Constantes.TYPE_LIVRET_TXT;
    }

    colorer(): string {
        return this.nombreObsolete > 0 ? 'danger' : '';
    }

}
