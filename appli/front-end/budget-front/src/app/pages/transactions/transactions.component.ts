import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Charge } from 'app/shared/classes-vo/charge';
import { Epargne } from 'app/shared/classes-vo/Epargne';
import { Produit } from 'app/shared/classes-vo/produit';
import { Revenue } from 'app/shared/classes-vo/Revenue';
import { Transaction } from 'app/shared/classes-vo/transaction';
import { Constantes } from 'app/shared/constantes';
import { CommunService } from 'app/shared/services/commun.service';
import { ProduitService } from 'app/shared/services/produit.service';
import { TransactionService } from 'app/shared/services/transaction.service';
import { PosteVo } from '../admin/classes-vo/poste-vo';
import { PosteService } from '../admin/services/poste.service';

@Component({
    selector: 'transactions-cmp',
    moduleId: module.id,
    templateUrl: 'transactions.component.html',
    styleUrls: ['transactions.css']
})

export class TransactionsComponent implements OnInit {
    formAjoutModifTransaction: FormGroup;
    listePoste: PosteVo[] = [];
    epargne: Epargne = new Epargne();
    charge: Charge = new Charge();
    revenue: Revenue = new Revenue();
    listeTransations: Transaction[];
    listeRevenues: Transaction[];
    listeChargesFixe: Transaction[]
    listeChargesVariables: Transaction[];
    date: Date = new Date();
    poste: PosteVo = new PosteVo();
    listeProduits: Produit[];
    listeChargesReelles: Transaction[];
    listeEpargne: Transaction[];


    constructor(private fc: FormBuilder,
        private readonly posteService: PosteService,
        private readonly transactionService: TransactionService,
        private readonly communService: CommunService,
        private readonly produitService: ProduitService) {
        this.formAjoutModifTransaction = this.fc.group(
            {
                id: [null],
                montant: [null, Validators.required],
                date: ["", Validators.required],
                fixe: [false],
                reelle: [false],
                mensuel: [false],
                poste: [null],
                type: ["CHARGE"],
                description: ["", Validators.required],
                produit: ["", Validators.required]
            }
        )
    }
    ngOnInit(): void {
        this.chargerListe();
    }


    chargerListeProduits() {
        this.produitService.listeProduits().subscribe(
            res => {
                this.listeProduits = res;
            }
        )
    }


    activerBoutonValider(): boolean {
        return !this.communService.formulaireValide(this.formAjoutModifTransaction);
    }


    chargerListe() {

        this.chargerListeProduits();
        this.chargerListePoste();
        this.chargerListeRevenue(true, this.date.getDate().toString(), this.date.getMonth().toString(), this.date.getFullYear().toString())
        this.chargerListeChargeFixe(true, this.date.getDate().toString(), this.date.getMonth().toString(), this.date.getFullYear().toString())
        this.chargerListeChargeVariables(true, this.date.getDate().toString(), this.date.getMonth().toString(), this.date.getFullYear().toString())
        this.chargerListeChargeReelles(true, this.date.getDate().toString(), this.date.getMonth().toString(), this.date.getFullYear().toString());
        this.chargerListeEpargne(true, this.date.getDate().toString(), this.date.getMonth().toString(), this.date.getFullYear().toString());

    }


    valider() {
        this.charge.dateTemp = new Date(this.formAjoutModifTransaction.value.date);
        this.charge.montant = this.formAjoutModifTransaction.value.montant;
        this.charge.description = this.formAjoutModifTransaction.value.description;
        this.charge.fixe = this.formAjoutModifTransaction.value.fixe;
        this.charge.mensuel = this.formAjoutModifTransaction.value.mensuel;
        this.charge.reelle = this.formAjoutModifTransaction.value.reelle;
        this.charge.poste = new PosteVo();
        this.charge.produit = new Produit();
        this.charge.produit.produitTechnicalId = this.formAjoutModifTransaction.value.produit;
        this.charge.poste.technicalId = this.formAjoutModifTransaction.value.poste;


        this.epargne.montant = this.formAjoutModifTransaction.value.montant;
        this.epargne.description = this.formAjoutModifTransaction.value.description;
        this.epargne.fixe = this.formAjoutModifTransaction.value.fixe;
        this.epargne.mensuel = this.formAjoutModifTransaction.value.mensuel;
        this.epargne.reelle = this.formAjoutModifTransaction.value.reelle;
        this.epargne.produit = new Produit();
        this.epargne.dateTemp = new Date(this.formAjoutModifTransaction.value.date);
        this.epargne.produit.produitTechnicalId = this.formAjoutModifTransaction.value.produit;

        this.revenue.montant = this.formAjoutModifTransaction.value.montant;
        this.revenue.description = this.formAjoutModifTransaction.value.description;
        this.revenue.fixe = this.formAjoutModifTransaction.value.fixe;
        this.revenue.mensuel = this.formAjoutModifTransaction.value.mensuel;
        this.revenue.reelle = this.formAjoutModifTransaction.value.reelle;
        this.revenue.produit = new Produit();
        this.revenue.dateTemp = new Date(this.formAjoutModifTransaction.value.date);
        this.revenue.produit.produitTechnicalId = this.formAjoutModifTransaction.value.produit;

        switch (this.formAjoutModifTransaction.value.type) {
            case Constantes.TYPE_CHARGE: {
                this.validerCharge(this.charge);
                break;
            }

            case Constantes.TYPE_EPARGNE: {
                this.validerEpargne(this.epargne);
                break;
            }

            case Constantes.TYPE_REVENUE: {
                this.validerRevenue(this.revenue);
                break;
            }
        }

        this.formAjoutModifTransaction.reset();
        this.formAjoutModifTransaction.value.id = null;
        this.formAjoutModifTransaction.value.produit = null;
        this.formAjoutModifTransaction.value.poste = null;


    }

    afficherPoste(): boolean {
        return this.formAjoutModifTransaction.value.type === Constantes.TYPE_CHARGE;
    }

    validerEpargne(epargne: Epargne) {
        this.transactionService.ajouterModifierEpargne(epargne).subscribe(

            r => {
                this.chargerListe();
                this.msgInfo();
            }
        )

    }

    validerCharge(charge: Charge) {
        console.log((charge))
        this.transactionService.ajouterModifierCharge(charge).subscribe(
            r => {
                this.chargerListe();
                this.msgInfo();
            }
        )

    }

    validerRevenue(revenue: Revenue) {

        this.transactionService.ajouterModifierRevenue(revenue).subscribe(
            r => {
                this.chargerListe();
                this.msgInfo();
            }
        )

    }

    msgInfo() {
        const message = 'L\'enregistrement a bien été créé.'
        this.communService.genererNotification(message, Constantes.SUCCESS, Constantes.TOP, Constantes.RIGHT);
    }

    chargerListePoste() {
        this.posteService.listePostes().subscribe(
            res => this.listePoste = res
        )
    }

    chargerListeRevenue(estAnnee: boolean, jour: string, mois: string, annee: string) {

        this.transactionService.recupererListeRevenueParDate(estAnnee, jour, mois, annee).subscribe(
            res => {
                this.listeRevenues = res
            }
        )
    }

    chargerListeChargeFixe(estAnnee: boolean, jour: string, mois: string, annee: string) {

        this.transactionService.recupererListeChargesFixeParDate(estAnnee, jour, mois, annee).subscribe(
            res => {
                this.listeChargesFixe = res
            }
        )
    }

    chargerListeChargeReelles(estAnnee: boolean, jour: string, mois: string, annee: string) {

        this.transactionService.recupererListeChargesReellesParDate(estAnnee, jour, mois, annee).subscribe(
            res => {
                this.listeChargesReelles = res
            }
        )
    }

    chargerListeEpargne(estAnnee: boolean, jour: string, mois: string, annee: string) {

        this.transactionService.recupererListeChargesEpargneParDate(estAnnee, jour, mois, annee).subscribe(
            res => {
                this.listeEpargne = res
            }
        )
    }

    chargerListeChargeVariables(estAnnee: boolean, jour: string, mois: string, annee: string) {

        this.transactionService.recupererListeChargesVariablesParDate(estAnnee, jour, mois, annee).subscribe(
            res => {
                this.listeChargesVariables = res
            }
        )
    }

    supprimer(id: number) {
        this.transactionService.supprimerproduitParId(id).subscribe(
            res => this.chargerListe()
        )
    }

    convertNumberToFormatString(number: number): string {
        return this.communService.numberToFormatlocale(number);
    }

}
