import { Produit } from "./produit";

export class Transaction {
    transactionTechnicalId: number;

    montant: number;

    description: string;

    date: Date;

    dateTemp: Date;

    composteur: Date;

    fixe: boolean;

    mensuel: boolean;

    reelle: boolean;

    produit: Produit;
}
