import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { Charge } from '../classes-vo/charge';
import { Epargne } from '../classes-vo/Epargne';
import { Revenue } from '../classes-vo/Revenue';
import { Transaction } from '../classes-vo/transaction';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  path = `${environment.path}transaction`;

  constructor(private http: HttpClient, private route: Router) { }

  ajouterModifierCharge(charge: Charge) {
    return this.http.post<Charge>(this.path + '/charge', charge);
  }

  ajouterModifierRevenue(revenue: Revenue) {
    return this.http.post<Revenue>(this.path + '/revenue', revenue);
  }

  ajouterModifierEpargne(epargne: Epargne) {
    return this.http.post<Epargne>(this.path + '/epargne', epargne);
  }


  listeTransaction() {
    return this.http.get<Transaction[]>(this.path);
  }

  supprimerproduitParId(id: number) {
    return this.http.delete(this.path + "/" + id);
  }

  recupereproduitParId(id: number) {
    return this.http.get<Transaction>(this.path + "/" + id);
  }

  calculerChargeFixe(estAnnee: boolean, jour: string, mois: string, annee: string) {
    return this.http.get<number>(this.path + "/" + "chargeFixe" + "/" + estAnnee + "/" + jour + "/" + mois + "/" + annee);
  }

  calculerChargeVar(estAnnee: boolean, jour: string, mois: string, annee: string) {
    return this.http.get<number>(this.path + "/" + "chargeVar" + "/" + estAnnee + "/" + jour + "/" + mois + "/" + annee);
  }

  calculerChargeVarReelle(estAnnee: boolean, jour: string, mois: string, annee: string) {
    return this.http.get<number>(this.path + "/" + "chargeRelle" + "/" + estAnnee + "/" + jour + "/" + mois + "/" + annee);
  }

  calculerRevenue(estAnnee: boolean, jour: string, mois: string, annee: string) {
    return this.http.get<number>(this.path + "/" + "revenue" + "/" + estAnnee + "/" + jour + "/" + mois + "/" + annee);
  }

  calculerEpargne(estAnnee: boolean, jour: string, mois: string, annee: string) {
    return this.http.get<number>(this.path + "/" + "epargne" + "/" + estAnnee + "/" + jour + "/" + mois + "/" + annee);
  }
  recupererListeChargesFixes(annee: string) {
    return this.http.get<number[]>(this.path + "/" + "charge/fixe" + "/" + annee);
  }
  recupererListeChargesVar(annee: string) {
    return this.http.get<number[]>(this.path + "/" + "charge/variables" + "/" + annee);
  }

  recupererListeChargesReelles(annee: string) {
    return this.http.get<number[]>(this.path + "/" + "charge/reelles" + "/" + annee);
  }

  recupererListeRevenueParDate(estAnnee: boolean, jour: string, mois: string, annee: string) {
    return this.http.get<Transaction[]>(this.path + "/" + "revenue/pardate" + "/" + estAnnee + "/" + jour + "/" + mois + "/" + annee);
  }

  recupererListeChargesFixeParDate(estAnnee: boolean, jour: string, mois: string, annee: string) {
    return this.http.get<Transaction[]>(this.path + "/" + "charge/fixe/pardate" + "/" + estAnnee + "/" + jour + "/" + mois + "/" + annee);
  }

  recupererListeChargesVariablesParDate(estAnnee: boolean, jour: string, mois: string, annee: string) {
    return this.http.get<Transaction[]>(this.path + "/" + "charge/pardate/var" + "/" + estAnnee + "/" + jour + "/" + mois + "/" + annee);
  }

  recupererListeChargesReellesParDate(estAnnee: boolean, jour: string, mois: string, annee: string) {
    return this.http.get<Transaction[]>(this.path + "/" + "charge/pardate/reelle" + "/" + estAnnee + "/" + jour + "/" + mois + "/" + annee);
  }

  recupererListeChargesEpargneParDate(estAnnee: boolean, jour: string, mois: string, annee: string) {
    return this.http.get<Transaction[]>(this.path + "/" + "charge/pardate/epargne" + "/" + estAnnee + "/" + jour + "/" + mois + "/" + annee);
  }

  recupererListeChargesParDate(estAnnee: boolean, jour: string, mois: string, annee: string) {
    return this.http.get<Charge[]>(this.path + "/" + "charge/pardate/tot" + "/" + estAnnee + "/" + jour + "/" + mois + "/" + annee);
  }

  recupererListeChargesParPOsteParDate(estAnnee: boolean, jour: string, mois: string, annee: string, id: number) {
    return this.http.get<Transaction[]>(this.path + "/" + "charge/parposte/pardate" + "/" + estAnnee + "/" + jour + "/" + mois + "/" + annee + "/" + id);
  }

}
