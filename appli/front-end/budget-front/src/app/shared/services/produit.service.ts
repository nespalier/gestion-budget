import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { Produit } from '../classes-vo/produit';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {

  path = `${environment.path}produit`;

  constructor(private http: HttpClient, private route: Router) { }

  ajouterModifierCompte(produitVo: Produit) {
    return this.http.post<Produit>(this.path + '/compte', produitVo);
  }

  ajouterModifierLivret(produitVo: Produit) {
    return this.http.post<Produit>(this.path + '/livret', produitVo);
  }

  listeProduits() {
    return this.http.get<Produit[]>(this.path);
  }

  supprimerproduitParId(id: number) {
    return this.http.delete(this.path + "/" + id);
  }

  recupereproduitParId(id: number) {
    return this.http.get<Produit>(this.path + "/" + id);
  }

  calculerSolde(estMois: boolean, estAnnee: boolean, typePorduit: string, jour: string, mois: string, annee: string) {
    return this.http.get<number>(this.path + "/" + estMois + "/" + estAnnee + "/" + typePorduit + "/" + jour + "/" + mois + "/" + annee);
  }

}
