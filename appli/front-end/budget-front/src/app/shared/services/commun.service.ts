import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { listenerCount } from 'process';
import { Constantes } from '../constantes';
import { Mois } from '../mois';

@Injectable({
  providedIn: 'root'
})
export class CommunService {

  constructor(private readonly toastr: ToastrService) { }
  genererNotification(message: string, type: string, positiony: string, positionx: string) {

    switch (type) {
      case Constantes.INFO:
        this.toastr.info(
          '<span data-notify="icon" class="nc-icon nc-bell-55"></span><span data-notify="message">' +
          message + '</span>',
          "",
          {
            timeOut: 4000,
            closeButton: true,
            enableHtml: true,
            toastClass: "alert alert-info alert-with-icon",
            positionClass: "toast-" + positiony + "-" + positionx
          }
        );
        break;
      case Constantes.SUCCESS:
        this.toastr.success(
          '<span data-notify="icon" class="nc-icon nc-bell-55"></span><span data-notify="message">' +
          message + '</span>',
          "",
          {
            timeOut: 4000,
            closeButton: true,
            enableHtml: true,
            toastClass: "alert alert-success alert-with-icon",
            positionClass: "toast-" + positiony + "-" + positionx
          }
        );
        break;
      case Constantes.WARNING:
        this.toastr.warning(
          '<span data-notify="icon" class="nc-icon nc-bell-55"></span><span data-notify="message">Welcome to <b>Paper Dashboard Angular</b> - a beautiful bootstrap dashboard for every web developer.</span>',
          "",
          {
            timeOut: 4000,
            closeButton: true,
            enableHtml: true,
            toastClass: "alert alert-warning alert-with-icon",
            positionClass: "toast-" + positiony + "-" + positionx
          }
        );
        break;
      case Constantes.ERROR:
        this.toastr.error(
          '<span data-notify="icon" class="nc-icon nc-bell-55"></span><span data-notify="message">Welcome to <b>Paper Dashboard Angular</b> - a beautiful bootstrap dashboard for every web developer.</span>',
          "",
          {
            timeOut: 4000,
            enableHtml: true,
            closeButton: true,
            toastClass: "alert alert-danger alert-with-icon",
            positionClass: "toast-" + positiony + "-" + positionx
          }
        );
        break;
      default:
        break;
    }
  }

  formulaireValide(formGroup: FormGroup): boolean {
    return formGroup.valid;
  }

  titleCaseWord(word: string) {
    if (!word) return word;
    return word[0].toUpperCase() + word.substr(1).toLowerCase();
  }

  getMois(mois: number): string {
    return Mois[mois].valeur;
  }

  getMoisLibelle(mois: number): string {
    return Mois[mois].libelle;
  }

  getMoisLibellePartirValeur(valeur: string): string {
    let resultat: string = '';
    for (const m of Mois) {
      if (m.valeur === valeur) {
        resultat = m.libelle;
      }
    }
    return resultat;
  }

  getListeMois() {
    return Mois;
  }

  getListeJour(): number[] {
    let liste: number[] = [];
    for (let i = 1; i <= 31; i++) {
      liste.push(i);
    }
    return liste;
  }

  numberToFormatlocale(number: number): string {
    return number.toLocaleString("fr-FR", { style: "currency", currency: "EUR" });
  }

}
